﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewForceDirection : MonoBehaviour
{

    void OnDrawGizmosSelected(){
        AreaEffector2D areaEffector2D = GetComponent<AreaEffector2D>();
        float angle = Mathf.Deg2Rad*areaEffector2D.forceAngle;
        Vector3 add = new Vector3(Mathf.Cos(angle),Mathf.Sin(angle));
        Gizmos.DrawLine(transform.position,transform.position+add);
    }
}
