﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float maxSpeed;
    public int clippingWidth = 20; // width at the edges where the scrolling should already occur (in screen units)
    public bool mouseMovementActive = true;

    [HideInInspector]
    public float cameraSpeedMultiplier;

    private Vector2 direction;

    private Vector2 lastDirection;

    void Awake(){
        direction = new Vector2(0f, 0f);
        lastDirection = new Vector2();
        cameraSpeedMultiplier = 1;
    }

    void Update(){
        SetDirection();
        Move();
    }

    // set moving direction based on mouse position on screen / arrow keys
    private void SetDirection(){
        direction.x = 0; direction.y = 0;

        // mouse controls
        if(mouseMovementActive){
            Vector3 mousePos = Input.mousePosition;
            if(!(mousePos.x <= Screen.width && mousePos.x >= 0 && mousePos.y <= Screen.height && mousePos.y >= 0)) return;
            
            if(mousePos.x >= Screen.width - clippingWidth){
                direction.x = 1 * Mathf.Abs((Screen.width - clippingWidth) - mousePos.x) / clippingWidth;
            } 
            if(mousePos.x <= 0 + clippingWidth){
                direction.x = -1 * (Mathf.Abs(clippingWidth - mousePos.x)/clippingWidth);
            }
            if(mousePos.y >= Screen.height - clippingWidth){
                direction.y = 1 * Mathf.Abs((Screen.height - clippingWidth) - mousePos.y) / clippingWidth;
            } 
            if(mousePos.y <= 0 + clippingWidth){
                direction.y = -1 * (Mathf.Abs(clippingWidth - mousePos.y)/clippingWidth);
            }
        }

        // keyboard controls
        int tempX = 0;
        int tempY = 0;
        if(Input.GetKey (KeyCode.A)) tempX -= 1;
        if(Input.GetKey (KeyCode.D)) tempX += 1;
        if(Input.GetKey (KeyCode.W)) tempY += 1;
        if(Input.GetKey (KeyCode.S)) tempY -= 1;
        direction.x = tempX; 
        direction.y = tempY;
        direction.Normalize();

        //Debug.Log("mouse: " + mousePos + ", dir: " + direction);
    }

    private void Move(){
        Vector2 newDirection = Vector2.Lerp(lastDirection,direction,0.1f);//camera smoothing
        lastDirection = newDirection;
        newDirection*=maxSpeed*cameraSpeedMultiplier;
        transform.position += new Vector3(newDirection.x, newDirection.y, 0f)*Time.deltaTime;
    }
}
