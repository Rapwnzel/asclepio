﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// container class to store tutorial steps
public class TutorialStep : MonoBehaviour
{
    public string title;
    public Sprite image;
    [TextArea(5, 8)]
    public string explanation;
    public bool shown = false;
    [Tooltip("Chain the next Step to this Step. Leave blank if not wanted.")]
    public TutorialStep chainNextTutorial;
}
