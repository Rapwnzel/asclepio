﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawner : MonoBehaviour
{
    public float despawnDelay = 0.5f;

    private Heart heart;
    private bool shouldDespawn = false;

    void Start(){
        heart = GetComponent<Heart>();
    }

    public void SetDespawn(bool sD){
        shouldDespawn = sD;
    }

    void OnTriggerEnter2D(Collider2D other) {
        switch (other.tag){
            case "bloodcell":
                if(shouldDespawn){
                    Destroy(other.gameObject, despawnDelay);
                }
                break;
            case "bacteria":
                heart.DamageByBacterium();
                break;
            case "virus":
                if(other.gameObject.GetComponent<Virus>().active){
                    heart.DamageByVirus();
                }
                break;
        }
    }
}
