﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    [Tooltip("Use this to turn off the tutorial")]
    public bool active = true;

    [Header("Tutorial UI elements")]
    public GameObject uiPanel;
    public Text uiTitle;
    public Image uiImage;
    public Text uiExplanation;

    private TutorialStep[] steps;
    private TutorialStep currentStep;

    void Awake(){
        steps = GetComponentsInChildren<TutorialStep>();
        active = false;
        if(PlayerPrefs.GetInt("tutorial", 1) == 1){
            active = true;
        }
        if(active) Trigger(0);
    }

    void Start(){
        
    }

    // Use those to trigger tutorial steps

    public void TriggerNext(){
        if(!active) return;
        for(int i=0; i<steps.Length; i++) {
            if(steps[i].Equals(currentStep) && !steps[i].shown && i+1<steps.Length){
                currentStep = steps[i+1];
                UpdateUI();
                ToggleUI(true);
            } 
        }
    }

    public void Trigger(int index){
        if(!active) return;
        if(index < steps.Length && steps[index] != null){
            if(steps[index].shown) return;
            currentStep = steps[index];
            UpdateUI();
            ToggleUI(true);
        }
    }

    public void Trigger(string title){
        if(!active) return;
        foreach (TutorialStep step in steps) {
            if(step.title.Equals(title) && !step.shown){
                currentStep = step;
                UpdateUI();
                ToggleUI(true);
            } 
        }
    }

    public void Trigger(TutorialStep targetStep){
        if(!active) return;
        foreach (TutorialStep step in steps) {
            if(step.Equals(targetStep) && !step.shown){
                currentStep = step;
                UpdateUI();
                ToggleUI(true);
            } 
        }
    }

    // triggered by OK button

    public void OnOKButton(){
        ToggleUI(false);
        currentStep.shown = true;
        if(currentStep.chainNextTutorial != null){
            Trigger(currentStep.chainNextTutorial);
        }
    }

    // internal functions

    private void UpdateUI(){
        uiTitle.text = currentStep.title;
        uiImage.sprite = currentStep.image;
        uiExplanation.text = currentStep.explanation;
    }

    private void ToggleUI(bool show){
        uiPanel.SetActive(show);
        if(show){ 
            Time.timeScale = 0.0f; 
        } else {
            Time.timeScale = 1.0f;
        }
    }
}
