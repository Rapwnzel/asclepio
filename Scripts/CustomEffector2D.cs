﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomEffector2D : MonoBehaviour
{

    public bool useGlobalAngle = false;
    public float forceAngle = 0;
    [Range(0,180)]
    public float angleVariation = 0;
    public float angleVariationSpeed = 1;
    public float forceMagnitude = 1;
    public float forceVariation = 0;
    
    public bool showAngleGizmo = true;

    public EffectorSelection2D forceTarget;

    private List<Collider2D> inEffector;

    float[] lastAngleVar;
    float[] nextAngleVar;
    float timeLeftInInterpolation = 0;

    // Start is called before the first frame update
    void Start()
    {
        inEffector = new List<Collider2D>();
        lastAngleVar = new float[5];
        nextAngleVar = new float[5];
    }

    void FixedUpdate(){
        if (timeLeftInInterpolation<=0){
            timeLeftInInterpolation+=angleVariationSpeed;
            for (int i = 0; i < lastAngleVar.Length; i++){
                lastAngleVar[i] = nextAngleVar[i];
                nextAngleVar[i] = angleVariation*Random.Range(-1f,1);
            }
        }
        float t = 1-timeLeftInInterpolation/angleVariationSpeed;
        timeLeftInInterpolation-=Time.fixedDeltaTime;

        Collider2D[] asArray = inEffector.ToArray();
        for (int i = 0; i < inEffector.Count; i++)
        {
            Collider2D item = asArray[i];
            if(item == null){
                inEffector.Remove(item);
                continue;
            }

            float forceAmount = forceMagnitude + Random.Range(-1f,1f)*forceVariation;

            int idx = i%lastAngleVar.Length;
            float angleVar = Mathf.SmoothStep(lastAngleVar[idx],nextAngleVar[idx],t);
            float angle = (useGlobalAngle?forceAngle:forceAngle+transform.rotation.eulerAngles.z) + angleVar;

            angle = angle*Mathf.Deg2Rad;
            Vector2 force = new Vector2(forceAmount*Mathf.Cos(angle),forceAmount*Mathf.Sin(angle));

            if (forceTarget == EffectorSelection2D.Rigidbody)
            {
                item.attachedRigidbody.AddForce(force);
            }else{
                item.attachedRigidbody.AddForceAtPosition(force,item.offset+new Vector2(item.transform.position.x,item.transform.position.y));
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col){
        Rigidbody2D rb = col.attachedRigidbody;
        if (rb!=null){
            inEffector.Add(col);
        }
    }
    
    void OnTriggerExit2D(Collider2D col){
        if(inEffector.Contains(col)){
            inEffector.Remove(col);
        }
    }

    void OnDrawGizmosSelected(){
        if(showAngleGizmo){
            float angle = (useGlobalAngle?forceAngle:forceAngle+transform.rotation.eulerAngles.z)*Mathf.Deg2Rad;
            Vector3 add = new Vector3(Mathf.Cos(angle),Mathf.Sin(angle));
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position,transform.position+add*1.3f);
            Gizmos.color = Color.green;
            add = new Vector3(Mathf.Cos(angle+angleVariation*Mathf.Deg2Rad),Mathf.Sin(angle+angleVariation*Mathf.Deg2Rad));
            Gizmos.DrawLine(transform.position,transform.position+add);
            add = new Vector3(Mathf.Cos(angle-angleVariation*Mathf.Deg2Rad),Mathf.Sin(angle-angleVariation*Mathf.Deg2Rad));
            Gizmos.DrawLine(transform.position,transform.position+add);
        }
    }
}
