﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjurySpawner : MonoBehaviour
{
    public float cooldown = 8f;
    public float minCooldown = 4f;
    [Tooltip("cooldown change per minute")]
    public float cooldownChangeRate = 0.5f;

    [Tooltip("GameObjects with a Script of type SpawnPoint.")]
    public SpawnPoint[] spawnPoints; // first the spawnPoint, second if occupied

    [Tooltip("Injury Prefabs.")]
    public GameObject[] injuries;

    private float lastInjury;

    void Awake(){
        for(int i=0; i<spawnPoints.Length; i++){
            spawnPoints[i].occupied = false;
        }
        lastInjury = Time.time;
    }

    void FixedUpdate(){
        if(Time.time > lastInjury + cooldown){
            lastInjury = Time.time;
            SpawnInjury();
        }
        cooldown -= cooldownChangeRate/(50*60);
        if(cooldown < minCooldown){
            cooldown = minCooldown;
        }
    }

    private void SpawnInjury(){
        SpawnPoint point = GetRandomSpawnPoint();
        if(point == null) return;
        point.occupied = true;

        GameObject temp = Instantiate(GetRandomInjury(), point.transform.position, point.transform.rotation);
        temp.GetComponent<Injury>().SetSpawnPoint(point);
    }

    private GameObject GetRandomInjury(){
        return injuries[Random.Range(0, injuries.Length)];
    }

    private SpawnPoint GetRandomSpawnPoint(){
        List<SpawnPoint> validSpawnPoints = new List<SpawnPoint>();
        foreach (SpawnPoint item in spawnPoints)
        {
            if(!item.occupied) validSpawnPoints.Add(item);
        }
        if(validSpawnPoints.Count == 0) return null;
        return validSpawnPoints[Random.Range(0, validSpawnPoints.Count)];
    }
}
