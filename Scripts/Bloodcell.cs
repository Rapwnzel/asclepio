﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloodcell : MonoBehaviour
{
    public float maxAngularVelocity;
    public int damage = 1;

    void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-maxAngularVelocity/2, maxAngularVelocity/2);
        // trigger tutorial if golden
        if(damage>=3) GameObject.Find("TutorialManager").GetComponent<TutorialManager>().Trigger("Golden blood cells");
    }
}
