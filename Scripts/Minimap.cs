﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    public GameObject cameraIndicator;
    public GameObject bodyIndicator;

    public Transform worldBodyBottomLeft;
    public Transform worldBodyTopRight;
    public Transform minimapBodyBottomLeft;
    public Transform minimapBodyTopRight;

    void Update(){
        UpdateCameraPosition();
    }

    private void UpdateCameraPosition(){
        // camera center pos in worldspace bekommen
        Vector3 cameraCenter = Camera.main.transform.position;

        // prozentsatz x und y ermitteln
        float percentX = (cameraCenter.x - worldBodyBottomLeft.position.x) / (worldBodyTopRight.position.x - worldBodyBottomLeft.position.x);
        float percentY = (cameraCenter.y - worldBodyBottomLeft.position.y) / (worldBodyTopRight.position.y - worldBodyBottomLeft.position.y);
        
        // make sure it never leaves the minimap
        if(percentX < 0) percentX = 0;
        if(percentX > 1) percentX = 1;
        if(percentY < 0) percentY = 0;
        if(percentY > 1) percentY = 1;
        Vector2 percentCameraWorld = new Vector2(percentX, percentY);
        
        // prozentwert auf camera position berechnen
        float x = minimapBodyBottomLeft.position.x + (minimapBodyTopRight.position.x - minimapBodyBottomLeft.position.x) * percentX;
        float y = minimapBodyBottomLeft.position.y + (minimapBodyTopRight.position.y - minimapBodyBottomLeft.position.y) * percentY;
        cameraIndicator.transform.position = new Vector3(x, y, cameraIndicator.transform.position.z);
    }
}
