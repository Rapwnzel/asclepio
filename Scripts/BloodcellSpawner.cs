﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodcellSpawner : MonoBehaviour
{
    public float spawnsPerSec;
    public LayerMask mask;
    public float spawnPreventionRadius = 0.5f;
    public GameObject bloodcell;
    public Transform spawnPoint;

    private float spawnCooldown;
    private float lastSpawn;

    void Awake()
    {
        lastSpawn = Time.time;
        spawnCooldown = 1f / spawnsPerSec;
    }

    void FixedUpdate()
    {
        SpawnCell();
    }

    void SpawnCell(){
        if(Time.time > lastSpawn + spawnCooldown && IsFree()){
            lastSpawn = Time.time;
            GameObject bc = Instantiate(bloodcell, spawnPoint.position, spawnPoint.rotation);
            GetComponent<Heart>().addBloodCell(bc);
        }
    }

    private bool IsFree(){
        Collider2D[] colliders = Physics2D.OverlapCircleAll(spawnPoint.transform.position, spawnPreventionRadius, mask);
        if(colliders.Length > 0){
            return false;
        }
        return true;
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(spawnPoint.transform.position, spawnPreventionRadius);
    }
}
