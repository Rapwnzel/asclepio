﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    [Min(0)]
    public float maxPressure = 5;
    [Min(0)]
    public float basePressure = 3;
    [Min(0)]
    public float baseBPM = 60;
    [Min(0)]
    public float passiveHealRate = 1/10f;
    [Min(0)]
    public float healDelay = 10;

    public int maxBloodCells = 200;
    public int minBloodCells = 100;
    public float timeToMinBloodCells = 600;

    public Manager manager;

    private float currentBeatDuration=0; // time this beat takes in total
    private float nextBeat=0; //time in seconds when the next beat should start;

    private float currentPressure;
    private float BPM;
    private float timeSinceLastDamage;

    private List<GameObject> allBloodCells;

    void Start(){
        BPM = baseBPM;
        timeSinceLastDamage = 100;
        allBloodCells = new List<GameObject>();
    }

    void Update(){
        float multi = (2-manager.getCurrentHealth());
        BPM = multi*baseBPM;
        Animator anim = GetComponentInChildren<Animator>();
        anim.speed = multi;

        timeSinceLastDamage+=Time.deltaTime;
        if(timeSinceLastDamage>healDelay){
            float healAmount = passiveHealRate*Time.deltaTime;
            manager.ModifyHealth(healAmount);
        }

        UpdateBloodCells();
    }

    private void UpdateBloodCells(){
        int bloodCellCount = Mathf.RoundToInt(Time.time<timeToMinBloodCells?Mathf.Lerp(maxBloodCells,minBloodCells,Time.time/timeToMinBloodCells):minBloodCells);

        allBloodCells.RemoveAll(item => item == null);
        GetComponent<Despawner>().SetDespawn(allBloodCells.Count>bloodCellCount);
        allBloodCells.RemoveAll(item => item == null);
        BloodcellSpawner[] bloodcellSpawners = GetComponents<BloodcellSpawner>();
        for (int i = 0; i < bloodcellSpawners.Length; i++){
            bloodcellSpawners[i].enabled = allBloodCells.Count<bloodCellCount;
        }
    }

    public void addBloodCell(GameObject bc){
        allBloodCells.Add(bc);
    }

    void FixedUpdate(){
        nextBeat -= Time.fixedDeltaTime;

        if (nextBeat<=0){
            currentBeatDuration = 60f/BPM; //in seconds
            nextBeat+=currentBeatDuration;
        }

        float normalizedTimeSinceStart = (currentBeatDuration-nextBeat)/currentBeatDuration;
        float addedPressure = 0;

        if (normalizedTimeSinceStart<0.333f){
            addedPressure = -Mathf.Pow(4.5f*normalizedTimeSinceStart-1,2)+1;
        }else{
            addedPressure = -1.2f*normalizedTimeSinceStart+1.2f;
        }
        currentPressure = basePressure+(maxPressure-basePressure)*addedPressure;
    }

    public float GetCurrentPressure(){
        return currentPressure;
    }

    public void DamageByBacterium(){
        timeSinceLastDamage = 0;
        manager.ModifyHealth(-0.5f);
    }

    public void DamageByVirus(){
        timeSinceLastDamage = 0;
        manager.ModifyHealth(-1f);
    }
}
