﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedLimiter : MonoBehaviour
{
    public float maxVelocity;
    private Rigidbody2D rb;

    private PointEffector2D p;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        bool shouldLimit = !(p!=null&&p.enabled);
        if(shouldLimit && rb.velocity.sqrMagnitude>maxVelocity*maxVelocity){
            rb.velocity = rb.velocity.normalized*maxVelocity;
        }
    }

    public void setReference(PointEffector2D reference){
        p=reference;
    }
}
