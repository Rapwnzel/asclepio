﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject mouseForceEffect;

    public LayerMask mask;
    public Texture2D mouseNormal;
    public Texture2D mouseHover;

    private Vector3 mousePos;
    private PointEffector2D pointEffector2D;

    // Start is called before the first frame update
    void Awake()
    {
        pointEffector2D = GetComponent<PointEffector2D>();
        mouseForceEffect.SetActive(false);
        Cursor.SetCursor(mouseNormal, Vector2.zero, CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = Input.mousePosition;
        if(Input.GetMouseButton(1)){
            pointEffector2D.enabled = true;
            mouseForceEffect.SetActive(true);
            Camera.main.GetComponent<CameraControl>().cameraSpeedMultiplier = .7f;
        } else {
            pointEffector2D.enabled = false;
            mouseForceEffect.SetActive(false);
            Camera.main.GetComponent<CameraControl>().cameraSpeedMultiplier = 1;
        }

        if(IsFree()){
            Cursor.SetCursor(mouseNormal, Vector2.zero, CursorMode.ForceSoftware);
        } else {
            Cursor.SetCursor(mouseHover, Vector2.zero, CursorMode.ForceSoftware);
        }

        ProcessClick();
        Vector3 worldPos = mainCamera.ScreenToWorldPoint(mousePos);
        worldPos.z=0;
        transform.position = worldPos;
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.gameObject.tag == "bloodcell")
        {
            col.gameObject.GetComponent<SpeedLimiter>().setReference(pointEffector2D);
        }
    }

    void OnTriggerExit2D(Collider2D col){
        if (col.gameObject.tag == "bloodcell")
        {
            col.gameObject.GetComponent<SpeedLimiter>().setReference(null);
        }
    }

    private bool IsFree(){
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f, mask);
        if(colliders.Length > 0){
            return false;
        }
        return true;
    }

    private void ProcessClick(){
        if(Input.GetMouseButtonDown(0)){
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f, mask);
            if(colliders.Length == 0) return;
            if(colliders.Length > 0){
                GameObject target = colliders[0].gameObject;
                if(target.GetComponent<IClickable>() != null){
                    target.GetComponent<IClickable>().Clicked();
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        // Gizmos.color = Color.yellow;
        // Gizmos.DrawWireSphere(transform.position, 0.3f);
    }
}
