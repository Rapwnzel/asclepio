﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Toggle tutorial;
    public Toggle sound;
    public Text highscore;

    private bool tutorialState;
    private bool soundState;

    public void Start(){
        // Load Player Prefs
        Time.timeScale = 1.0f;
        tutorialState = PlayerPrefs.GetInt("tutorial", 1) == 1;
        tutorial.isOn = tutorialState;
        soundState = PlayerPrefs.GetInt("sound", 1) == 1;
        sound.isOn = soundState;
        highscore.text = "Highscore: " + PlayerPrefs.GetInt("highscore", 0);
    }

    public void Play(){
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void Quit(){
        Application.Quit();
    }

    public void ToggleTutorial(bool toggled){
        tutorialState = toggled;
        PlayerPrefs.SetInt("tutorial", BoolToInt(tutorialState));
    }

    public void ToggleSound(bool toggled){
        soundState = toggled;
        PlayerPrefs.SetInt("sound", BoolToInt(soundState));
    }

    private int BoolToInt(bool val){
        if(val) return 1;
        return 0;
    }
    
}
