﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bacterium : MonoBehaviour, IClickable
{
    public int maxHP;
    public int mouseDamage;
    public GameObject destroyEffectEnemy;
    public GameObject destroyEffectBlood;
    public GameObject damageEffect;

    private int hp;

    void Start()
    {
        hp = maxHP;
    }

    public void Clicked(){
        Damage(mouseDamage);
    }

    // get damaged from bloodcells
    void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "bloodcell"){
            Damage(other.gameObject.GetComponent<Bloodcell>().damage);
            Destroy(Instantiate(destroyEffectBlood, other.gameObject.transform.position, Quaternion.identity), 1.2f);
            Destroy(other.gameObject);
        }
    }

    private void Damage(int damage){
        hp -= damage;
        for(int i=0; i<damage; i++){
            Destroy(Instantiate(damageEffect, transform.position, Quaternion.identity), 1.1f);
        }
        if(hp <= 0){
            Destroy(Instantiate(destroyEffectEnemy, transform.position, Quaternion.identity), 1.2f);
            Destroy(gameObject);
        }
    }
}
