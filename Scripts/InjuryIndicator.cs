﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjuryIndicator : MonoBehaviour
{
    private Injury injury;
    private float inset = 2f;

    void Update(){
        CheckDelete();
        if(injury == null) return;
        UpdatePosition();
    }

    public void SetInjury(Injury inj){
        injury = inj;
    }

    private void UpdatePosition(){
        // calculate in world space
        float cameraHeight = Mathf.Abs(Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f, 0f)).y - Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f)).y);
        float cameraWidth = Mathf.Abs(Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f, 0f)).x - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f)).x);
        Vector2 cameraDims = new Vector2(cameraWidth, cameraHeight);
        Vector2 indicatorDims = cameraDims-2*inset*Vector2.one;

        Vector3 centerPoint = Camera.main.transform.position;
        Vector2 centerPoint2 = new Vector2(centerPoint.x, centerPoint.y);
        Vector3 targetPoint = injury.transform.position;
        Vector2 targetPoint2 = new Vector2(targetPoint.x, targetPoint.y);

        // calculate if visible or not (injury in camera bounds)
        Rect r = new Rect(centerPoint2-cameraDims/2,cameraDims);
        if (r.Contains(targetPoint2)){
            GetComponent<SpriteRenderer>().enabled = false;
            return;
        }else{
            GetComponent<SpriteRenderer>().enabled = true;
        }

        float angle = Mathf.Atan2((targetPoint2.y-centerPoint2.y), (targetPoint2.x-centerPoint2.x));
        angle = angle<0?angle+2*Mathf.PI:angle;// convert to range (0,2pi) instead of (-pi,pi)
        float atan = Mathf.Atan2(indicatorDims.y, indicatorDims.x);
        if(angle >= atan && angle < Mathf.PI - atan){
            //top
            transform.position = new Vector3(centerPoint2.x + indicatorDims.y/ (2 * Mathf.Tan(angle)), centerPoint2.y + indicatorDims.y/2, 0f);
        }else if (angle >= Mathf.PI - atan && angle < Mathf.PI + atan){
            //left
            transform.position = new Vector3(centerPoint2.x - indicatorDims.x/2, centerPoint2.y - (indicatorDims.x/2 * Mathf.Tan(angle)), 0f);
        }else if (angle >= Mathf.PI + atan && angle < 2*Mathf.PI - atan){
            //bottom
            transform.position = new Vector3(centerPoint2.x - indicatorDims.y/ (2 * Mathf.Tan(angle)), centerPoint2.y - indicatorDims.y/2, 0f);
        }else{
            //right
            transform.position = new Vector3(centerPoint2.x + indicatorDims.x/2, centerPoint2.y + (indicatorDims.x/2 * Mathf.Tan(angle)), 0f);
        }

    }

    private void CheckDelete(){
        if(injury == null){
            Destroy(gameObject);
        }
    }

    
}
