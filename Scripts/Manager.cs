﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public float maxHealth;
    
    [Header("UI Elements")]
    public Slider healthBar;
    public Text uiTimeCounter;
    public GameObject defeatScreen;
    public GameObject pauseScreen;
    public float health;

    private float counterStartTime;

    void Start(){
        ModifyHealth(maxHealth);
        counterStartTime = Time.time;
    }

    // use this to damage or heal
    public void ModifyHealth(float delta){
        if(delta >= 0){
            // healing
        } else {
            // damage
        }

        health += delta;

        if(health <= 0) {
            // dead
            defeatScreen.SetActive(true);
            SaveHighscore();
            Time.timeScale = 0f;
        }
        if(health > maxHealth) {
            // fully healed
            health = maxHealth;
        }
        healthBar.value = health / maxHealth;
        if(delta < 0) healthBar.gameObject.GetComponent<Animator>().SetTrigger("damaged");
    }

    //normalized to (0,1)
    public float getCurrentHealth(){
        return health/maxHealth;
    }

    public void ToMenu(){
        SaveHighscore();
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

    public void TogglePauseScreen(bool pause){
        pauseScreen.SetActive(pause);
        Time.timeScale = pause ? 0f : 1f;
    }

    public void TogglePauseScreen(){
        pauseScreen.SetActive(!pauseScreen.activeSelf);
        Time.timeScale = pauseScreen.activeSelf ? 0f : 1f;
    }

    void Update(){
        uiTimeCounter.text = "" + Mathf.Floor(Time.time - counterStartTime);
        if(Input.GetKeyDown(KeyCode.Escape)){
            TogglePauseScreen();
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.blue;
        foreach (Transform child in GetComponentsInChildren<Transform>())
        {
            Gizmos.DrawWireSphere(child.transform.position, 0.3f); 
        }
    }

    private void SaveHighscore(){
        int score = (int) Mathf.Floor(Time.time - counterStartTime);
        int oldScore = PlayerPrefs.GetInt("highscore", 0);
        if(score > oldScore) PlayerPrefs.SetInt("highscore", score);
    }
}
