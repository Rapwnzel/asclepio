﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("sound", 1) != 1){
            GetComponent<AudioSource>().mute = true;
        }   
    }
}
