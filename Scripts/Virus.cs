﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : MonoBehaviour, IClickable
{
    public float initialCooldown = 10f;
    public int maxHP;
    public int mouseDamage;
    public GameObject destroyEffectEnemy;
    public GameObject destroyEffectBlood;
    public GameObject damageEffect;
    public bool active;

    private int hp;
    private float started;
    private Animator animator;

    void Start(){
        started = Time.time;
        animator = GetComponent<Animator>();
        hp = maxHP;
        GameObject.Find("TutorialManager").GetComponent<TutorialManager>().Trigger("Viruses");
    }

    void FixedUpdate(){
        if(!active && Time.time > started + initialCooldown){
            animator.SetBool("active", true);
            active = true;
        }

        if(active){
            // TODO
        }
    }

    // kill bloodcells
    void OnCollisionEnter2D(Collision2D other) {
        if(active && other.gameObject.tag == "bloodcell"){
            Damage(other.gameObject.GetComponent<Bloodcell>().damage);
            Destroy(Instantiate(destroyEffectBlood, other.gameObject.transform.position, Quaternion.identity), 1.2f);
            Destroy(other.gameObject);
        }
        if(active && other.gameObject.tag == "virus"){
            Virus otherVirus = other.gameObject.GetComponent<Virus>();
            if(otherVirus.active){
                FixedJoint2D joint = gameObject.AddComponent<FixedJoint2D>();
                joint.dampingRatio = 0.5f;
                joint.connectedBody = other.gameObject.GetComponent<Rigidbody2D>();
            }
        }
    }

    public void Clicked(){
        if(active){
            Damage(mouseDamage);
        }
    }

    private void Damage(int damage){
        hp -= damage;
        for(int i=0; i<damage; i++){
            Destroy(Instantiate(damageEffect, transform.position, Quaternion.identity), 1.1f);
        }
        if(hp <= 0){
            Destroy(Instantiate(destroyEffectEnemy, transform.position, Quaternion.identity), 1.2f);
            Destroy(gameObject);
        }
    }
}