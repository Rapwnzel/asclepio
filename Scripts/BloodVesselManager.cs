﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodVesselManager : MonoBehaviour
{
    private List<CustomEffector2D> bloodVesselSegments;
    public Heart heart;
    // Start is called before the first frame update
    void Start()
    {
        bloodVesselSegments = new List<CustomEffector2D>();
        GameObject[] gos =  GameObject.FindGameObjectsWithTag("bloodVessel");
        foreach (var item in gos){
            bloodVesselSegments.Add(item.GetComponent<CustomEffector2D>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        float pressure = heart.GetCurrentPressure();
        foreach (var item in bloodVesselSegments)
        {
            item.forceMagnitude = pressure;
        }
    }    
}
