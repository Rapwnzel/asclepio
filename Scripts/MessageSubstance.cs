﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageSubstance : MonoBehaviour, IClickable
{
    public int lifetime = 30;

    private float started;
    private SpriteRenderer spriteRenderer;
    private Injury injury;
    private GameObject injuryIndicator;

    void Start(){
        started = Time.time;
        spriteRenderer = GetComponent<SpriteRenderer>();
        GameObject.Find("TutorialManager").GetComponent<TutorialManager>().Trigger("Messenger Substances");
    }

    void FixedUpdate(){
        if(Time.time >= started + lifetime) Destroy(gameObject);
        float transparency = 1f - (Time.time-started)/lifetime;
        spriteRenderer.color = new Color(1f, 1f, 1f, transparency);
        if(injury == null || injury.revealed) Destroy(gameObject);
    }

    public void SetInjury(Injury inj){
        injury = inj;
    }

    public void SetInjuryIndicator(GameObject inj){
        injuryIndicator = inj;
    }

    public void Clicked(){
        injury.revealed = true;
        GameObject ind = Instantiate(injuryIndicator, new Vector3(1000f, 1000f, 0f), Quaternion.identity);
        ind.GetComponent<InjuryIndicator>().SetInjury(injury);
        Destroy(gameObject);
    }
}
