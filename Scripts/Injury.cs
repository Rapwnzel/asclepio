﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Injury : MonoBehaviour
{
    public int maxHealth = 10;
    public float cooldown;
    public float cooldownMessengerSubstance;
    public float spawnPreventionRadius;
    public LayerMask mask;
    public GameObject messengerSubstance;
    public GameObject[] enemyTypes;
    public GameObject healEffect;
    public bool revealed; // if clicked on a messageSubstance
    public GameObject injuryIndicator;
    public GameObject damageEffectGreen;

    private float lastSpawn;
    private float lastMessengerSpawn;
    private int health;
    private SpawnPoint mySpawn;

    void Start(){
        health = maxHealth;
        revealed = false;
    }

    void FixedUpdate(){
        SpawnEnemy();
        SpawnMessengerSubstance();
    }

    // called by InjurySpawner during initialization
    public void SetSpawnPoint(SpawnPoint p){
        mySpawn = p;
    }

    private void SpawnEnemy(){
        if(Time.time > lastSpawn + cooldown && IsFree()){
            Instantiate(GetRandomEnemy(), transform.position, transform.rotation);
            lastSpawn = Time.time;
        }
    }

    private GameObject GetRandomEnemy(){
        return enemyTypes[Random.Range(0, enemyTypes.Length)];
    }

    private void SpawnMessengerSubstance(){
        if(Time.time > lastMessengerSpawn + cooldownMessengerSubstance && IsFree() && !revealed){
            GameObject sub = Instantiate(messengerSubstance, transform.position, transform.rotation);
            sub.GetComponent<MessageSubstance>().SetInjury(this);
            sub.GetComponent<MessageSubstance>().SetInjuryIndicator(injuryIndicator);
            lastMessengerSpawn = Time.time;
        }
    }

    private bool IsFree(){
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, spawnPreventionRadius, mask);
        if(colliders.Length > 0){
            return false;
        }
        return true;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "bloodcell"){
            Destroy(Instantiate(healEffect, other.gameObject.transform.position, Quaternion.identity), 1.2f);
            Damage(other.gameObject.GetComponent<Bloodcell>().damage);
            Destroy(other.gameObject);
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, spawnPreventionRadius);
    }

    private void Damage(int damage){
        health -= damage;
        for(int i=0; i<damage; i++){
            Destroy(Instantiate(damageEffectGreen, transform.position, Quaternion.identity), 1.1f);
        }
        if(health <= 0){
            health = 0;
            mySpawn.occupied = false;
            Destroy(gameObject);
        }
    }
}
