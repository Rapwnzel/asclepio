# asclepio

A game we made for ludum dare 46.
Just download the build you need, unpack it and enjoy our game!

Link to Builds: [Builds](https://gitlab.com/Rapwnzel/asclepio/-/tree/master/Builds)

Link to ludum dare page: [Ludum Dare - Asclepio](https://ldjam.com/events/ludum-dare/46/asclepio)